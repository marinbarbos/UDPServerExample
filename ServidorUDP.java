import java.io.IOException;
import java.net.*;


public class ServidorUDP {
    private DatagramSocket socketudp;
    private int porta;

    public ServidorUDP(int porta) throws SocketException, IOException {
        this.porta = porta;
        this.socketudp = new DatagramSocket(this.porta);
    }

    public static void main(String[] args) throws Exception {
        ServidorUDP cliente = new ServidorUDP(server port);
        cliente.listen();
    }

    private void listen() throws Exception {
        System.out.println("=== Server em => " + InetAddress.getLocalHost() + " ===");
        String str;

        while (true) {
            byte[] buffer = new byte[256];
            DatagramPacket pacote = new DatagramPacket(buffer, buffer.length);

            socketudp.receive(pacote);
            str = new String(pacote.getData()).trim();

            System.out.println("Pacote =>" + pacote.getAddress().getHostAddress() + ": " + str);
        }
    }
}