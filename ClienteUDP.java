import java.io.IOException;
import java.net.*;
import java.util.Scanner;


public class ClienteUDP {
    private DatagramSocket socketudp;
    private InetAddress enderecoServidor;
    private int porta;
    private Scanner scanner;

    private ClienteUDP(String enderecoServidor, int porta) throws IOException {
        this.enderecoServidor = InetAddress.getByName(enderecoServidor);
        this.porta = porta;
        socketudp = new DatagramSocket(this.porta);
        scanner = new Scanner(System.in);
    }

    public static void main(String[] args) throws NumberFormatException, IOException {
        ClienteUDP cliente = new ClienteUDP("server ip address", server port);
        System.out.println("=== Cliente em => " + InetAddress.getLocalHost() + " ===");
        cliente.start();
    }

    private int start() throws IOException {
        String strIn;
        while (true) {
            strIn = scanner.nextLine();
            DatagramPacket pacote = new DatagramPacket(strIn.getBytes(), strIn.getBytes().length, enderecoServidor, porta);
            this.socketudp.send(pacote);
        }
    }
}